//
//  WordScrambleApp.swift
//  WordScramble
//
//  Created by Drummer on 2022. 05. 08..
//

import SwiftUI

@main
struct WordScrambleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
