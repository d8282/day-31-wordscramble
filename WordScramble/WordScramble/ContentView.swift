import SwiftUI

struct ContentView: View {
    @State private var usedWords = [String]()
    @State private var rootWord = ""
    @State private var newWord = ""

    @State private var errorTitle = ""
    @State private var errorMessage = ""
    @State private var showingError = false

    private var userScore: Int {
        usedWords.count
    }

    var body: some View {
        NavigationView {
            VStack {
                List {
                    Section {
                        TextField("Enter word", text: $newWord)
                            .autocapitalization(.none)
                    }

                    Section {
                        ForEach(usedWords, id: \.self) { word in
                            HStack {
                                Text(word)
                                Spacer()
                                Image(systemName: "\(word.count).circle")
                            }
                        }
                    }
                }
                .listStyle(.insetGrouped)

                Text("Valid Words: \(userScore)")
            }
            .toolbar {
                Button(action: {
                    startGame()
                }) {
                    Image(systemName: "arrow.clockwise")
                        .renderingMode(.original)
                }
            }
            .navigationTitle(rootWord)
            .onSubmit(addNewWord)
            .onAppear(perform: startGame)
            .alert(errorTitle, isPresented: $showingError) {
                Button("OK", role: .cancel) {}
            } message: {
                Text(errorMessage)
            }
        }
    }

    func addNewWord() {
        let answer = newWord.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)

        guard answer.count > 3, answer != rootWord else { return }

        guard isOriginal(answer) else {
            wordError(title: "Word used already", message: "Be more original")
            return
        }

        guard isPossible(answer) else {
            wordError(title: "Word not possible", message: "You can't spell that word from '\(rootWord)'!")
            return
        }

        guard isReal(answer) else {
            wordError(title: "Word not recognized", message: "You can't just make them up, you know!")
            return
        }

        withAnimation {
            usedWords.insert(answer, at: .zero)
        }
        newWord = ""
    }

    func startGame() {
        if let startWordsURL = Bundle.main.url(forResource: "start", withExtension: "txt") {
            if let startWords = try? String(contentsOf: startWordsURL) {
                let allWords = startWords.components(separatedBy: "\n")
                rootWord = allWords.randomElement() ?? "normandy"
                usedWords.removeAll()
                return
            }
        }

        fatalError("start.txt is missing from the bundle")
    }

    func isOriginal(_ word: String) -> Bool {
        !usedWords.contains(word)
    }

    func isPossible(_ word: String) -> Bool {
        var tempWord = word

        for letter in word {
            if let pos = tempWord.firstIndex(of: letter) {
                tempWord.remove(at: pos)
            } else {
                return false
            }
        }

        return true
    }

    func isReal(_ word: String) -> Bool {
        let checker = UITextChecker()
        let range = NSRange(location: 0, length: word.utf16.count)
        let misspelledRange = checker.rangeOfMisspelledWord(in: word, range: range, startingAt: 0, wrap: false, language: "en")

        return misspelledRange.location == NSNotFound
    }

    func wordError(title: String, message: String) {
        errorTitle = title
        errorMessage = message
        showingError = true
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .preferredColorScheme(.dark)
    }
}
